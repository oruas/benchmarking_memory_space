import 'dart:io';
import 'dart:math';
import 'package:flutter/material.dart';
import 'package:temporaldb/platform.dart';
import 'package:tuple/tuple.dart';

import 'package:wakelock/wakelock.dart';
import 'package:path/path.dart';

import 'package:temporaldb/temporal_db_1d_factory.dart';
import 'package:temporaldb/models/temporal_model.dart';
import 'package:sqflite/sqflite.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const MyHomePage(title: 'Benchmarking memory space'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);
  final String title;
  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  /// Variable in which the size of the considered data base is stored.
  /// We consider the file in which the db is store on the main memory, not RAM.
  /// This value is expressed as Mb and is printed on the screen.
  var _fileSize = 0.0;

  /// Variable storing number of database insertions.
  int _dbInsertions = 0;

  /// The variable operation is used to prevent to launch two experiments
  /// at the same time. For simplicity we do not use token and assume sequential
  /// accesses to the variable. Values:
  /// -1 : interrupting the ongoing experiment
  /// 0 : default value, no operations are currently done
  /// 1 : XP on sqlite on random values
  /// 2 : FLI on random values
  /// 3 : SQLite on constant values
  /// 4 : FLI on constant values
  int _operation = 0;
  String _getNameXP(int nbOperation) {
    switch (nbOperation) {
      case -1:
        return 'Interrupting experiment...';
      case 0:
        return 'No experiment running';
      case 1:
        return 'SQLite on random values running';
      case 2:
        return 'FLI on random values running';
      case 3:
        return 'SQLite on constant values running';
      case 4:
        return 'FLI on constant values running';
      default:
        return 'Error on operation number';
    }
  }

  /// Directory where the results are stored
  static const _addResults = 'results/';

  /// Default number of insertions for each experiment
  static const _numberInsertions = 1000000;

  /// Default number of insertions between two measurements
  static const _stepMeasurement = 10000;

  @override
  void initState() {
    super.initState();

    /// The following line will enable the Android and iOS wakelock.
    Wakelock.enable();
  }

  void _writeFileFLI(TemporalModel m, String fileName) async {
    List<Tuple2<double, List<double>>> explicitModel =
        m.getModelsAndTimestamps();
    // List<int> stringBytes = utf8.encode(explicitModel.toString());
    // List<int> compressedBytes = gzip.encode(stringBytes);
    File file = await localDirFile('', fileName);
    // file.writeAsBytesSync(compressedBytes);
    file.writeAsStringSync(explicitModel.toString());
    return;
  }

  String _getNameResultsFile(String xp) {
    return xp + '--' + getTimestamp() + '.txt';
  }

  void _interruptXP() {
    _operation = -1;
    setState(() {
      _dbInsertions = 0;
    });
  }

  /// *********** SQLite Random ****************
  /// Insert 1,000,000 random values into a sqlite table and periodically
  /// print the size taken by the database on the disk.
  void _doSQLiteXPRandomDefault() {
    _doSQLiteRandomXP(_numberInsertions);
  }

  /// Insert random values into a sqlite table and periodically
  /// print the size taken by the database on the disk.
  void _doSQLiteRandomXP(int nbXp) async {
    debugPrint('Starting SQLite with random values');

    Random rd = Random(0);
    int timestamp = 0;

    /// Updating the variable _operation to prevent another xp to be launched
    if (_operation != 0) {
      return;
    }
    _operation = 1;

    ///Opening file for writing results
    String _resultsFileName = _getNameResultsFile('randomSQLResults');
    final File outputFile =
        await localDirFile(_addResults + 'RandomSQL/', _resultsFileName);
    debugPrint("Path to the file: ${outputFile.path}\n");
    // final File outputFile = await _localFile(_resultsFileName);
    outputFile.open(mode: FileMode.writeOnlyAppend);
    outputFile.writeAsString("NbValues\tFileSize\tTime\n");

    const _sqliteDBFileName = 'sqlite_database.db';

    ///opening the sqlite database
    await deleteDatabase(join(await getDatabasesPath(), _sqliteDBFileName));
    WidgetsFlutterBinding.ensureInitialized();
    final Future<Database> database = openDatabase(
      join(await getDatabasesPath(), _sqliteDBFileName),
      onCreate: (db, version) {
        return db.execute(
            "CREATE TABLE RDpoints(timestamp REAL PRIMARY KEY, value REAL)");
      },
      version: 1,
    );
    final Database db = await database;
    var dbBatch = db.batch();

    /// Starting the experiment
    Stopwatch stopwatch = Stopwatch()..start();
    int i;
    for (i = 0; i < nbXp; i++) {
      // timestamp = timestamp + 1 + rd.nextInt(100);
      timestamp = DateTime.now().microsecondsSinceEpoch;
      //     .millisecondsSinceEpoch;
      // await db.insert(
      dbBatch.insert(
        'RDpoints',
        {'timestamp': timestamp, 'value': getNextDouble(rd)},
        conflictAlgorithm: ConflictAlgorithm.replace,
      );

      setState(() {
        _dbInsertions += 1;
      });

      if (i % _stepMeasurement == 0) {
        await dbBatch.commit(noResult: true);
        dbBatch = db.batch();
        if (_operation == -1) {
          break;
        }
        debugPrint(i.toString());
        var file = File(join(await getDatabasesPath(), _sqliteDBFileName));
        var fileLength = (await file.length()) / 1000000;
        _fileSize = fileLength;
        setState(() {});
        // int? count = Sqflite.firstIntValue(await db.rawQuery('SELECT COUNT(*) FROM RDpoints'));
        // debugPrint('The SQLite table has $count entries');
        debugPrint("DB SQLite file size: $fileLength Mb");
        outputFile.writeAsString("$i\t$fileLength\t${stopwatch.elapsed}\n",
            mode: FileMode.append);
      }
    }
    dbBatch.commit(noResult: true);
    // int? count = Sqflite.firstIntValue(await db.rawQuery('SELECT COUNT(*) FROM RDpoints'));
    // debugPrint('The SQLite table has $count entries');

    var file = File(join(await getDatabasesPath(), _sqliteDBFileName));
    var fileLength = (await file.length()) / 1000000;
    _fileSize = fileLength;
    if (i == nbXp + 1) {
      i = nbXp;
    }
    outputFile.writeAsString("$i\t$fileLength\t${stopwatch.elapsed}\n",
        mode: FileMode.append);
    db.close();
    debugPrint("DB SQLite file size: $fileLength Mb");
    await deleteDatabase(join(await getDatabasesPath(), _sqliteDBFileName));
    _operation = 0;
    setState(() {
      _dbInsertions = 0;
    });
    debugPrint('SQLite with random values: done');
  }

  /// *********** FLI Random ****************
  /// Insert 1,000,000 random values into FLI and periodically
  /// print the size taken by the database on the disk.
  void _doFLIXPRandomDefault() {
    _doFLIRandomXP(_numberInsertions);
  }

  /// Insert random values into FLI and periodically
  /// print the size taken by the database on the disk.
  void _doFLIRandomXP(int nbXp) async {
    debugPrint('Starting FLI with random values');

    Random rd = Random(0);
    int timestamp;

    /// Updating the variable _operation to prevent another xp to be launched
    if (_operation != 0) {
      return;
    }
    _operation = 2;

    ///Opening file for writing results
    String _resultsFileName = _getNameResultsFile('randomFLIResults');
    final File outputFile =
        await localDirFile(_addResults + 'RandomFLI/', _resultsFileName);
    debugPrint("Path to the file: ${outputFile.path}\n");
    // final File outputFile = await _localFile(_resultsFileName);
    outputFile.open(mode: FileMode.writeOnlyAppend);
    outputFile.writeAsString("NbValues\tFileSize\tTime\n");

    const _fliDBFileName = 'fli_database.gzip';

    ///opening the FLI database
    final TemporalModel db = Factory();

    /// Starting the experiment
    Stopwatch stopwatch = Stopwatch()..start();
    int i;
    for (i = 0; i < nbXp; i++) {
      timestamp = DateTime.now().microsecondsSinceEpoch;
      db.add(timestamp.toDouble(), getNextDouble(rd));
      setState(() {
        _dbInsertions += 1;
      });
      if (i % _stepMeasurement == 0) {
        if (_operation == -1) {
          break;
        }
        debugPrint(i.toString());
        _writeFileFLI(db, _fliDBFileName);
        var file = await localDirFile('', _fliDBFileName);
        var fileLength = (await file.length()) / 1000000;
        // var fileLength = db.getSize()/1000000;
        _fileSize = fileLength;
        setState(() {});
        debugPrint("FLI file size: $fileLength Mb");
        outputFile.writeAsString("$i\t$fileLength\t${stopwatch.elapsed}\n",
            mode: FileMode.append);
      }
    }

    _writeFileFLI(db, _fliDBFileName);
    var file = await localDirFile('', _fliDBFileName);
    var fileLength = (await file.length()) / 1000000;
    // var fileLength = db.getSize()/1000000;
    _fileSize = fileLength;
    if (i == nbXp + 1) {
      i = nbXp;
    }
    outputFile.writeAsString("$i\t$fileLength\t${stopwatch.elapsed}\n",
        mode: FileMode.append);
    debugPrint("FLI file size: $fileLength Mb");
    await deleteDatabase(join(await getDatabasesPath(), _fliDBFileName));
    _operation = 0;
    setState(() {
      _dbInsertions = 0;
    });
    debugPrint('FLI with random values: done');
  }

  /// *********** SQLite Constant ****************
  /// Insert 1,000,000 random values into a sqlite table and periodically
  /// print the size taken by the database on the disk.
  void _doSQLiteXPConstantDefault() {
    _doSQLiteConstantXP(_numberInsertions);
  }

  /// Insert random values into a sqlite table and periodically
  /// print the size taken by the database on the disk.
  void _doSQLiteConstantXP(int nbXp) async {
    debugPrint('Starting SQLite with constant values');

    Random rd = Random(0);
    double value = getNextDouble(rd);
    int timestamp;

    /// Updating the variable _operation to prevent another xp to be launched
    if (_operation != 0) {
      return;
    }
    _operation = 3;

    ///Opening file for writing results
    String _resultsFileName = _getNameResultsFile('constantSQLResults');
    final File outputFile =
        await localDirFile(_addResults + 'ConstantSQL/', _resultsFileName);
    debugPrint("Path to the file: ${outputFile.path}\n");
    // final File outputFile = await _localFile(_resultsFileName);
    outputFile.open(mode: FileMode.writeOnlyAppend);
    outputFile.writeAsString("NbValues\tFileSize\tTime\n");

    const _sqliteDBFileName = 'sqlite_database.db';

    ///opening the sqlite database
    await deleteDatabase(join(await getDatabasesPath(), _sqliteDBFileName));
    WidgetsFlutterBinding.ensureInitialized();
    final Future<Database> database = openDatabase(
      join(await getDatabasesPath(), _sqliteDBFileName),
      onCreate: (db, version) {
        return db.execute(
            "CREATE TABLE RDpoints(timestamp REAL PRIMARY KEY, value REAL)");
      },
      version: 1,
    );
    final Database db = await database;
    var dbBatch = db.batch();

    /// Starting the experiment
    Stopwatch stopwatch = Stopwatch()..start();
    int i;
    for (i = 0; i < nbXp; i++) {
      timestamp = DateTime.now().microsecondsSinceEpoch;
      dbBatch.insert(
        'RDpoints',
        {'timestamp': timestamp, 'value': value},
        conflictAlgorithm: ConflictAlgorithm.replace,
      );
      setState(() {
        _dbInsertions += 1;
      });
      if (i % _stepMeasurement == 0) {
        await dbBatch.commit(noResult: true);
        dbBatch = db.batch();
        if (_operation == -1) {
          break;
        }
        debugPrint(i.toString());
        var file = File(join(await getDatabasesPath(), _sqliteDBFileName));
        var fileLength = (await file.length()) / 1000000;
        _fileSize = fileLength;
        setState(() {});
        debugPrint("DB SQLite file size: $fileLength Mb");
        outputFile.writeAsString("$i\t$fileLength\t${stopwatch.elapsed}\n",
            mode: FileMode.append);
      }
    }
    await dbBatch.commit(noResult: true);

    var file = File(join(await getDatabasesPath(), _sqliteDBFileName));
    var fileLength = (await file.length()) / 1000000;
    _fileSize = fileLength;
    if (i == nbXp + 1) {
      i = nbXp;
    }
    outputFile.writeAsString("$i\t$fileLength\t${stopwatch.elapsed}\n",
        mode: FileMode.append);
    db.close();
    debugPrint("DB SQLite file size: $fileLength Mb");
    await deleteDatabase(join(await getDatabasesPath(), _sqliteDBFileName));
    _operation = 0;
    setState(() {
      _dbInsertions = 0;
    });
    debugPrint('SQLite with constant values: done');
  }

  /// *********** FLI Constant ****************
  /// Insert 1,000,000 random values into FLI and periodically
  /// print the size taken by the database on the disk.
  void _doFLIXPConstantDefault() {
    _doFLIConstantXP(_numberInsertions);
  }

  /// Insert random values into FLI and periodically
  /// print the size taken by the database on the disk.
  void _doFLIConstantXP(int nbXp) async {
    debugPrint('Starting FLI with constant values');

    Random rd = Random(0);
    double value = getNextDouble(rd);
    int timestamp;

    /// Updating the variable _operation to prevent another xp to be launched
    if (_operation != 0) {
      return;
    }
    _operation = 4;

    ///Opening file for writing results
    String _resultsFileName = _getNameResultsFile('constantFLIResults.txt');
    final File outputFile =
        await localDirFile(_addResults + 'ConstantFLI/', _resultsFileName);
    debugPrint("Path to the file: ${outputFile.path}\n");
    // final File outputFile = await _localFile(_resultsFileName);
    outputFile.open(mode: FileMode.writeOnlyAppend);
    outputFile.writeAsString("NbValues\tFileSize\tTime\n");

    const _fliDBFileName = 'fli_database.gzip';

    ///opening the FLI database
    final TemporalModel db = Factory();

    /// Starting the experiment
    Stopwatch stopwatch = Stopwatch()..start();
    int i;
    for (i = 0; i < nbXp; i++) {
      timestamp = DateTime.now().microsecondsSinceEpoch;
      db.add(timestamp.toDouble(), value);
      setState(() {
        _dbInsertions += 1;
      });
      if (i % _stepMeasurement == 0) {
        if (_operation == -1) {
          break;
        }
        debugPrint(i.toString());
        _writeFileFLI(db, _fliDBFileName);
        var file = await localDirFile('', _fliDBFileName);
        var fileLength = (await file.length()) / 1000000;
        // var fileLength = db.getSize()/1000000;
        _fileSize = fileLength;
        setState(() {});
        debugPrint("FLI file size: $fileLength Mb");
        outputFile.writeAsString("$i\t$fileLength\t${stopwatch.elapsed}\n",
            mode: FileMode.append);
      }
    }

    _writeFileFLI(db, _fliDBFileName);
    var file = await localDirFile('', _fliDBFileName);
    var fileLength = (await file.length()) / 1000000;
    // var fileLength = db.getSize()/1000000;
    _fileSize = fileLength;
    if (i == nbXp + 1) {
      i = nbXp;
    }
    outputFile.writeAsString("$i\t$fileLength\t${stopwatch.elapsed}\n",
        mode: FileMode.append);
    debugPrint("FLI file size: $fileLength Mb");
    await deleteDatabase(join(await getDatabasesPath(), _fliDBFileName));
    _operation = 0;
    setState(() {
      _dbInsertions = 0;
    });
    debugPrint('FLI with constant values: done');
  }

  @override
  Widget build(BuildContext context) {
    const TextStyle btnTxtStyle = TextStyle(color: Colors.white);

    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: Container(
          margin: const EdgeInsets.all(20),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              /// Launch XP: storing random values with sql
              ElevatedButton(
                style: ElevatedButton.styleFrom(
                  backgroundColor: Colors.blue,
                ),
                onPressed: _doSQLiteXPRandomDefault,
                child: const Text(
                  'XP: Storing random values with SQLite',
                  style: btnTxtStyle,
                ),
              ),

              /// Launch XP: storing random values with FLI
              ElevatedButton(
                style: ElevatedButton.styleFrom(
                  backgroundColor: Colors.blue,
                ),
                onPressed: _doFLIXPRandomDefault,
                child: const Text(
                  'XP: Storing random values with FLI',
                  style: btnTxtStyle,
                ),
              ),

              /// Launch XP: storing constant values with sql
              ElevatedButton(
                style: ElevatedButton.styleFrom(
                  backgroundColor: Colors.blue,
                ),
                onPressed: _doSQLiteXPConstantDefault,
                child: const Text(
                  'XP: Storing constant values with SQLite',
                  style: btnTxtStyle,
                ),
              ),

              /// Launch XP: storing constant values with sql
              ElevatedButton(
                style: ElevatedButton.styleFrom(
                  backgroundColor: Colors.blue,
                ),
                onPressed: _doFLIXPConstantDefault,
                child: const Text(
                  'XP: Storing constant values with FLI',
                  style: btnTxtStyle,
                ),
              ),

              Container(
                margin: const EdgeInsets.symmetric(vertical: 10),
                child: Text(
                  _getNameXP(_operation),
                  style: Theme.of(context).textTheme.headlineSmall,
                  textAlign: TextAlign.center,
                ),
              ),
              Text(
                'The file has a size of $_fileSize Mb',
                style: Theme.of(context).textTheme.titleLarge,
              ),
              Text(
                'Database inserted entries: $_dbInsertions',
                style: Theme.of(context).textTheme.titleLarge,
              )
            ],
          ),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: _interruptXP,
        tooltip: 'Interrupt experiment',
        child: const Icon(
          Icons.clear_rounded,
          color: Colors.white,
        ),
        backgroundColor: Colors.red,
      ),
    );
  }
}
